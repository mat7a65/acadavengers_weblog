package de.awacademy.Academy_Pearls_Weblog.blogpost;

import javax.validation.constraints.*;

public class BlogPostDTO {

    @NotBlank
    private String title;
    @Size(min = 1, max = 500)
    private String text;

    public BlogPostDTO(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }
}
