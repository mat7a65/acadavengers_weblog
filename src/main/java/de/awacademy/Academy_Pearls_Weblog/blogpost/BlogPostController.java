package de.awacademy.Academy_Pearls_Weblog.blogpost;

import de.awacademy.Academy_Pearls_Weblog.postcomment.PostCommentService;
import de.awacademy.Academy_Pearls_Weblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class BlogPostController {

    private BlogPostService blogPostService;
    private PostCommentService postCommentService;

    @Autowired
    public BlogPostController(BlogPostService blogPostService, PostCommentService postCommentService) {
        this.blogPostService = blogPostService;
        this.postCommentService = postCommentService;
    }

    @GetMapping("/blogpost")
    public String blogpost(Model model) {
        model.addAttribute("blogpost", new BlogPostDTO("", ""));
        return "blogpost";
    }

    @PostMapping("/blogpost")
    public String blogpost(@Valid @ModelAttribute("blogpost") BlogPostDTO blogPostDTO,
                           BindingResult bindingResult,
                           @ModelAttribute("sessionUser") User sessionUser) {
        if (bindingResult.hasErrors()) {
            return "blogpost";
        }

        blogPostService.add(blogPostDTO, sessionUser);
        return "redirect:/";
    }

    @GetMapping("/blogpost/editblogpost/{blogpostId}")
    public String editBlogPost(Model model, @PathVariable Long blogpostId) {
        model.addAttribute("blogpost", blogPostService.getBlogPost(blogpostId));
        return "editblogpost";
    }

    @PostMapping("/blogpost/editblogpost/{blogpostId}")
    public String editBlogPost(@ModelAttribute("blogpost") BlogPostDTO blogPostDTO, @PathVariable Long blogpostId) {
        blogPostService.edit(blogPostDTO, blogpostId);
        return "redirect:/edited";
    }

    @GetMapping("/{blogpostId}/delete")
    public String deleteBlogPostAndPostComments(Model model, @PathVariable Long blogpostId) {
        model.addAttribute("blogpost", blogPostService.getBlogPost(blogpostId));
        return "editblogpost";
    }

    @PostMapping("/{blogpostId}/delete")
    public String deleteBlogPostAndPostComments(@PathVariable Long blogpostId) {
        postCommentService.deleteByBlogPostId(blogpostId);
        blogPostService.delete(blogpostId);
        return "redirect:/edited";
    }
}
