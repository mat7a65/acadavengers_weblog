package de.awacademy.Academy_Pearls_Weblog.blogpost;

import de.awacademy.Academy_Pearls_Weblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;

@Service
public class BlogPostService {

    private BlogPostRepository blogPostRepository;

    @Autowired
    public BlogPostService(BlogPostRepository blogPostRepository) {
        this.blogPostRepository = blogPostRepository;
    }

    public List<BlogPost> getAllBlogPostsSorted() {
        return blogPostRepository.findAllByOrderByPostedAtDesc();
    }

    public void add(BlogPostDTO blogPostDTO, User sessionUser) {
        BlogPost blogPost = new BlogPost(sessionUser, blogPostDTO.getTitle(), blogPostDTO.getText(), Instant.now());
        blogPostRepository.save(blogPost);
    }

    public BlogPost getBlogPost(Long blogPostId) {
        Optional<BlogPost> blogPost = blogPostRepository.findById(blogPostId);
        return blogPost.get();
    }

    public void edit(BlogPostDTO blogPostDTO, Long blogPostId) {
        BlogPost blogPost = blogPostRepository.findById(blogPostId).get();
        blogPost.setText(blogPostDTO.getText());
        blogPostRepository.save(blogPost);
    }

    public void delete(Long blogPostId) {
        Optional<BlogPost> blogPost = blogPostRepository.findById(blogPostId);
        blogPostRepository.delete(blogPost.get());
    }
}
