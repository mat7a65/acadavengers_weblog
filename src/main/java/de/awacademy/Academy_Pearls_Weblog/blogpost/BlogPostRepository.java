package de.awacademy.Academy_Pearls_Weblog.blogpost;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface BlogPostRepository extends CrudRepository<BlogPost, Long> {

    List<BlogPost> findAllByOrderByPostedAtDesc();

}
