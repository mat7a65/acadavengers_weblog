package de.awacademy.Academy_Pearls_Weblog.blogpost;

import de.awacademy.Academy_Pearls_Weblog.postcomment.PostComment;
import de.awacademy.Academy_Pearls_Weblog.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class BlogPost {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;

    private String title;
    private String text;
    private Instant postedAt;

    @OneToMany(mappedBy = "blogPost")
    private List<PostComment> postCommentList;

    public BlogPost() {
    }

    public BlogPost(User user, String title, String text, Instant postedAt) {
        this.user = user;
        this.title = title;
        this.text = text;
        this.postedAt = postedAt;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public List<PostComment> getCommentsOfPost() {
        return postCommentList;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

