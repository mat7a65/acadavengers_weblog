package de.awacademy.Academy_Pearls_Weblog;

import de.awacademy.Academy_Pearls_Weblog.blogpost.BlogPostService;
import de.awacademy.Academy_Pearls_Weblog.postcomment.PostCommentService;
import de.awacademy.Academy_Pearls_Weblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    private BlogPostService blogPostService;
    private PostCommentService postCommentService;

    @Autowired
    public HomeController(BlogPostService blogPostService, PostCommentService postCommentService) {
        this.blogPostService = blogPostService;
        this.postCommentService = postCommentService;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        model.addAttribute("allBlogPosts", blogPostService.getAllBlogPostsSorted());
        model.addAttribute("allPostComments", postCommentService.getAllPostCommentsSorted());
        return "home";
    }

}
