package de.awacademy.Academy_Pearls_Weblog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        return user.get();
    }

    public Boolean userExists(RegistrationDTO registration) {
        return userRepository.existsByUsername(registration.getUsername());
    }

    public void edit(Long userId) {
        User user = userRepository.findById(userId).get();
        user.setAdmin(true);
        userRepository.save(user);
    }

    public Optional<User> findByUsernameAndPassword(String name, String password) {
        return (this.userRepository.findByUsernameAndPassword(name, password));
    }

    public void addUser(User user) {
        this.userRepository.save(user);
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }


}
