package de.awacademy.Academy_Pearls_Weblog.user;

import org.springframework.data.repository.CrudRepository;

import java.util.*;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsernameAndPassword(String username, String password);

    boolean existsByUsername(String username);

    List<User> findAll();

}
