package de.awacademy.Academy_Pearls_Weblog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registration", new RegistrationDTO("", "", ""));
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("registration") RegistrationDTO registration,
                           BindingResult bindingResult) {
        if (!registration.getPassword1().equals(registration.getPassword2())) {
            bindingResult.addError(new FieldError(
                    "registration", "password2", "Stimmt nicht überein."));
        }

        if (userService.userExists(registration)) {
            bindingResult.addError(new FieldError(
                    "registration", "username", "Schon vergeben!"));
        }

        if (bindingResult.hasErrors()) {
            return "register";
        }

        User user = new User(registration.getUsername(), registration.getPassword1());
        userService.addUser(user);
        return "redirect:/registersuccess";
    }

    @GetMapping("/registersuccess")
    public String registersuccess() {
        return "registersuccess";
    }

    @GetMapping("/admin")
    public String admin(Model model) {
        model.addAttribute("userList", userService.getUsers());
        return "admin";
    }

    @GetMapping("/edituser/{userId}")
    public String makeUserAdmin(Model model, @PathVariable Long userId) {
        model.addAttribute("user", userService.getUser(userId));
        return "/edituser";
    }

    @PostMapping("/edituser/{userId}")
    public String makeUserAdmin(@PathVariable Long userId) {
        userService.edit(userId);
        return "redirect:/";
    }
}