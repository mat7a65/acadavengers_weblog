package de.awacademy.Academy_Pearls_Weblog.user;

import de.awacademy.Academy_Pearls_Weblog.blogpost.BlogPost;
import de.awacademy.Academy_Pearls_Weblog.postcomment.PostComment;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "user")
    private List<BlogPost> blogPosts;

    @OneToMany(mappedBy = "user")
    private List<PostComment> postcommentlist;

    private String username;
    private String password;

    private Boolean isAdmin;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.isAdmin = false;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }


    public List<BlogPost> getBlogPosts() {
        return blogPosts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setBlogPosts(List<BlogPost> blogPosts) {
        this.blogPosts = blogPosts;
    }

    public List<PostComment> getPostComments() {
        return postcommentlist;
    }

    public void setPostComments(List<PostComment> postComments) {
        this.postcommentlist = postComments;
    }

}
