package de.awacademy.Academy_Pearls_Weblog.session;

import org.springframework.data.repository.CrudRepository;
import java.time.Instant;
import java.util.Optional;

public interface SessionRepository extends CrudRepository<Session, String> {

    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt);

}
