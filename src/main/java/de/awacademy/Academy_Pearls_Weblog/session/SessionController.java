package de.awacademy.Academy_Pearls_Weblog.session;

import de.awacademy.Academy_Pearls_Weblog.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.*;
import java.time.Instant;
import java.util.Optional;

@Controller
public class SessionController {

    private SessionService sessionService;
    private UserService userService;

    @Autowired
    public SessionController(SessionService sessionService, UserService userService) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }

    @GetMapping("/edited")
    public String edited() {
        return "editsuccess";
    }


    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO("", ""));
        return "login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("login") LoginDTO login,
                        BindingResult bindingResult, HttpServletResponse response) {

        Optional<User> optionalUser = userService.findByUsernameAndPassword(login.getUsername(), login.getPassword());

        if (optionalUser.isPresent()) {
            Session session = new Session(optionalUser.get(), Instant.now().plusSeconds(7 * 24 * 60 * 60));
            sessionService.addSession(session);

            Cookie cookie = new Cookie("sessionId", session.getId());
            response.addCookie(cookie);

            return "redirect:/";
        }

        bindingResult.addError(new FieldError(
                "login", "password", "Login fehlgeschlagen."));
        return "login";
    }

    @PostMapping("/logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId,
                         HttpServletResponse response) {

        Optional<Session> session = sessionService.findByIdAndExpiresAtAfter(sessionId, Instant.now());
        session.ifPresent(currentSession -> sessionService.deleteSession(currentSession));

        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        return "redirect:/";
    }
}
