package de.awacademy.Academy_Pearls_Weblog.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.Instant;
import java.util.Optional;

@Service
public class SessionService {

    private SessionRepository sessionRepository;

    @Autowired
    public SessionService(SessionRepository sessionRepository){
        this.sessionRepository = sessionRepository;
    }

    public void addSession(Session session){
        this.sessionRepository.save(session);
    }

    public Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt){

        return(sessionRepository.findByIdAndExpiresAtAfter(id, expiresAt));
    }

    public void deleteSession(Session session){

        this.sessionRepository.delete(session);
    }
}
