package de.awacademy.Academy_Pearls_Weblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademyPearlsWeblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademyPearlsWeblogApplication.class, args);
	}
}