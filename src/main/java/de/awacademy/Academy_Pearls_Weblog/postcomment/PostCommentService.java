package de.awacademy.Academy_Pearls_Weblog.postcomment;

import de.awacademy.Academy_Pearls_Weblog.blogpost.*;
import de.awacademy.Academy_Pearls_Weblog.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;

@Service
public class PostCommentService {

    private PostCommentRepository postCommentRepository;
    private BlogPostRepository blogPostRepository;
    private UserRepository userRepository;

    @Autowired
    public PostCommentService(PostCommentRepository postCommentRepository,
                              BlogPostRepository blogPostRepository, UserRepository userRepository) {
        this.postCommentRepository = postCommentRepository;
        this.blogPostRepository = blogPostRepository;
        this.userRepository = userRepository;
    }

    public List<PostComment> getAllPostCommentsSorted() {
        return postCommentRepository.findAllByOrderByPostedAtAsc();
    }

    //Kommentar ergänzen nur in Abhängigkeit zu User und BlogPost
    public void add(PostComment postComment, Long blogPostId, Long userId, Instant postedAt) {
        Optional<BlogPost> blogPost = blogPostRepository.findById(blogPostId);
        Optional<User> sessionUser = userRepository.findById(userId);
        postComment.setBlogPost(blogPost.get());
        postComment.setUser(sessionUser.get());
        postComment.setPostedAt(postedAt);
        postCommentRepository.save(postComment);
    }

    public void delete(PostComment postComment) {
        postCommentRepository.delete(postComment);
    }

    public void deleteByBlogPostId(Long blogpostId) {
        List<PostComment> postCommentList = postCommentRepository.findAllByBlogPost_Id(blogpostId);
        postCommentRepository.deleteAll(postCommentList);

    }

    public PostComment getComment(Long postcommentId, Long userId) {
        Optional<PostComment> postComment = postCommentRepository.findByIdAndUser_Id(postcommentId, userId);
        return postComment.get();
    }

}
