package de.awacademy.Academy_Pearls_Weblog.postcomment;

import de.awacademy.Academy_Pearls_Weblog.blogpost.BlogPost;
import de.awacademy.Academy_Pearls_Weblog.user.User;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.Instant;

@Entity
public class PostComment {

    @Id
    @GeneratedValue
    private Long id;

    @Size(min = 1, max = 250)
    private String text;

    private Instant postedAt;

    @ManyToOne
    private BlogPost blogPost;

    @ManyToOne
    private User user;

    public PostComment() {
    }

    public PostComment(User user, String text, Instant postedAt) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public BlogPost getBlogPost() {
        return blogPost;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public void setBlogPost(BlogPost blogPost) {
        this.blogPost = blogPost;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setText(String text) {
        this.text = text;
    }
}
