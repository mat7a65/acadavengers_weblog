package de.awacademy.Academy_Pearls_Weblog.postcomment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class PostCommentController {

    private PostCommentService postCommentService;

    @Autowired
    public PostCommentController(PostCommentService postCommentService) {
        this.postCommentService = postCommentService;
    }

    //Kommentare tauchen das erste Mal im Home auf (keine extra Methode nötig)

    //Formular für Neueintrag von Kommentaren
    @GetMapping("/{userId}/blogId/{blogPostId}/postcomment")
    public String addComment(Model model, @PathVariable Long blogPostId, @PathVariable Long userId) {
        model.addAttribute("postcomment", new PostComment());
        model.addAttribute("blogPostId", blogPostId);
        model.addAttribute("userId", userId);
        return "commentonpost";
    }

    //Aufnahme von eingegebenen Kommentar nach Klicken von Submit
    @PostMapping("/{userId}/blogId/{blogPostId}/postcomment")
    public String addComment(@Valid @ModelAttribute("postcomment") PostComment postComment,
                             BindingResult bindingResult,
                             @PathVariable Long blogPostId,
                             @PathVariable Long userId) {
        if (bindingResult.hasErrors()) {
            return "commentonpost";
        }
        postCommentService.add(postComment, blogPostId, userId, Instant.now());
        return "redirect:/edited";
    }

    @GetMapping("/{userId}/{postcommentId}/deletecomment")
    public String deleteComment(@PathVariable Long postcommentId,
                                @PathVariable Long userId) {
        postCommentService.delete(postCommentService.getComment(postcommentId, userId));
        return "redirect:/edited";
    }

}
