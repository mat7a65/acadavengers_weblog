package de.awacademy.Academy_Pearls_Weblog.postcomment;

import org.springframework.data.repository.CrudRepository;
import java.util.*;

public interface PostCommentRepository extends CrudRepository<PostComment, Long> {

    List<PostComment> findAllByOrderByPostedAtAsc();
    Optional<PostComment> findByIdAndUser_Id(Long postcommentId, Long userId);
    List<PostComment> findAllByBlogPost_Id(Long blogpostId);

}
