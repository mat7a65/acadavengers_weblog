package de.awacademy.Academy_Pearls_Weblog;

import de.awacademy.Academy_Pearls_Weblog.blogpost.*;
import de.awacademy.Academy_Pearls_Weblog.postcomment.PostCommentService;
import de.awacademy.Academy_Pearls_Weblog.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AcademyPearlsWeblogApplicationTests {

	@Autowired
	PostCommentService postCommentService;
	@Autowired
	BlogPostService blogPostService;

	public AcademyPearlsWeblogApplicationTests(PostCommentService postCommentService, BlogPostService blogPostService) {
		this.blogPostService = blogPostService;
		this.postCommentService = postCommentService;
	}

	@Test
	void contextLoads() {
	}

	@Test
	void testAddBlogPost(){
		BlogPostDTO blogPostDTO = new BlogPostDTO("Test", "Test-Text");
		User user = new User("testuser", "passwort");
		blogPostService.add(blogPostDTO, user);
		Assertions.assertEquals("Test", blogPostDTO.getTitle());
		Assertions.assertEquals("Test-Text", blogPostDTO.getText());
		Assertions.assertEquals("testuser", user.getUsername());
		Assertions.assertEquals("password", user.getPassword());
	}




}
